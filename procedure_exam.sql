CREATE OR REPLACE PROCEDURE Gustavo_Lozano_proc(
    nss IN EMPLEADO.NSS_SUPERV%TYPE
)
AS
    CURSOR empleados(v_nss IN VARCHAR2)
    IS  
        SELECT NSS_SUPERV, APELLIDO, SALARIO 
        FROM EMPLEADO
        WHERE NSS_SUPERV = v_nss;
        list empleados%ROWTYPE;
    BEGIN
        FOR list IN empleados(nss)
        LOOP
            INSERT INTO Gustavo_Lozano_Inc(NSS_SUPERV, APELLIDO, ANTERIOR, NUEVO)
            VALUES(list.NSS_SUPERV, list.APELLIDO, list.SALARIO, (list.Salario + (list.Salario * 0.10)));
        END LOOP;
    END;
/